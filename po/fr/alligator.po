# SPDX-FileCopyrightText: 2020, 2021, 2022, 2023 Xavier Besnard <xavier.besnard@kde.org>
# Xavier Besnard <xavier.besnard@kde.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: alligator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-02 00:38+0000\n"
"PO-Revision-Date: 2023-10-04 12:39+0200\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xavier Besnard"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Xavier.besnard@neuf.fr"

#: database.cpp:67 database.cpp:312
#, kde-format
msgid "Default"
msgstr "Par défaut"

#: database.cpp:68 database.cpp:313
#, kde-format
msgid "Default Feed Group"
msgstr "Groupe de fil d'actualité par défaut"

#: fetcher.cpp:241
#, kde-format
msgid "Invalid XML"
msgstr "Données XML non valables"

#: fetcher.cpp:243
#, kde-format
msgid "No parser accepted the XML"
msgstr "Aucun analyseur n'a accepté les données « XML »"

#: fetcher.cpp:245
#, kde-format
msgid "Error while parsing feed"
msgstr "Erreur durant l'analyse du fil d'actualités"

#: main.cpp:79 qml/AlligatorGlobalDrawer.qml:55
#, kde-format
msgid "Alligator"
msgstr "Alligator"

#: main.cpp:81
#, kde-format
msgid "Feed Reader"
msgstr "Lecteur de fil d'actualité"

#: main.cpp:83
#, kde-format
msgid "© 2020-2023 KDE Community"
msgstr "© 2020 - 2023 Communauté de KDE"

#: main.cpp:84
#, kde-format
msgid "Tobias Fella"
msgstr "Tobias Fella"

#: main.cpp:93
#, kde-format
msgid "RSS/Atom Feed Reader"
msgstr "Lecteur de fil d'actualités (RSS)"

#: main.cpp:95
#, kde-format
msgid "Adds a new feed to database."
msgstr "Ajoute un nouveau fil de discussions à la base de données."

#: main.cpp:96
#, kde-format
msgid "feed URL"
msgstr "URL du fil de discussions"

#: qml/AddFeedDialog.qml:19
#, kde-format
msgid "Add Feed"
msgstr "Ajouter un flux"

#: qml/AddFeedDialog.qml:25
#, kde-format
msgid "Url"
msgstr "URL"

#: qml/AddFeedDialog.qml:31
#, kde-format
msgid "Mark entries as read"
msgstr "Marquer les entrées comme lues"

#: qml/AddFeedDialog.qml:36
#, kde-format
msgctxt "@action:button"
msgid "Add Feed"
msgstr "Ajouter un flux"

#: qml/AddFeedDialog.qml:43
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Annuler"

#: qml/AlligatorGlobalDrawer.qml:89 qml/AlligatorGlobalDrawer.qml:96
#, kde-format
msgid "All Feeds"
msgstr "Tous les fils d'actualité"

#: qml/AlligatorGlobalDrawer.qml:154 qml/SettingsPage.qml:21
#, kde-format
msgid "Settings"
msgstr "Configuration"

#: qml/AlligatorGlobalDrawer.qml:165
#, kde-format
msgid "Manage Feeds"
msgstr "Gérer les fils d'actualité"

#: qml/AlligatorGlobalDrawer.qml:177
#, kde-format
msgid "About"
msgstr "A propos"

#: qml/EditFeedDialog.qml:15
#, kde-format
msgid "Edit Feed"
msgstr "Modifier un fil d'actualité"

#: qml/EditFeedDialog.qml:35
#, kde-format
msgid "Display Name:"
msgstr "Nom d'affichage :"

#: qml/EditFeedDialog.qml:45
#, kde-format
msgid "Group:"
msgstr "Groupe :"

#: qml/EntryListDelegate.qml:64
#, kde-format
msgctxt "by <author(s)>"
msgid "by"
msgstr "par"

#: qml/EntryListPage.qml:45
#, kde-format
msgid "Refresh"
msgstr "Rafraîchir"

#: qml/EntryListPage.qml:52
#, kde-format
msgid "Details"
msgstr "Détails"

#: qml/EntryListPage.qml:67
#, kde-format
msgid "Expand sidebar"
msgstr "Développer la barre latérale"

#: qml/EntryListPage.qml:67
#, kde-format
msgid "Collapse sidebar"
msgstr "Réduire la barre latérale"

#: qml/EntryListPage.qml:77
#, kde-format
msgid "All Entries"
msgstr "Toutes les entrées"

#: qml/EntryListPage.qml:85
#, kde-format
msgid "Error"
msgstr "Erreur"

#: qml/EntryListPage.qml:86 qml/EntryListPage.qml:99
#, kde-format
msgid "Error (%1): %2"
msgstr "Erreur (%1) : %2"

#: qml/EntryListPage.qml:97
#, kde-format
msgid "No unread entries available"
msgstr "Aucune entrée non lue disponible."

#: qml/EntryListPage.qml:97
#, kde-format
msgid "No entries available"
msgstr "Aucune entrée disponible."

#: qml/EntryListPage.qml:141
#, kde-format
msgid "All"
msgstr "Tout"

#: qml/EntryListPage.qml:147
#, kde-format
msgid "Unread"
msgstr "Non lu"

#: qml/EntryPage.qml:63
#, kde-format
msgid "Open in Browser"
msgstr "Ouvrir dans un navigateur"

#: qml/FeedDetailsPage.qml:20
#, kde-format
msgctxt "<Feed Name> - Details"
msgid "%1 - Details"
msgstr "%1 - Détails"

#: qml/FeedDetailsPage.qml:36
#, kde-format
msgctxt "by <author(s)>"
msgid "by %1"
msgstr "par %1"

#: qml/FeedDetailsPage.qml:44
#, kde-format
msgid "Subscribed since: %1"
msgstr "Abonné depuis : %1"

#: qml/FeedDetailsPage.qml:47
#, kde-format
msgid "last updated: %1"
msgstr "Mis à jour depuis : %1"

#: qml/FeedDetailsPage.qml:50
#, kde-format
msgid "%1 posts, %2 unread"
msgstr "%1 messages, %2 non lus"

#: qml/FeedGroupDialog.qml:23
#, kde-format
msgid "Feed Group"
msgstr "Groupes de fils d'actualités"

#: qml/FeedGroupDialog.qml:43
#, kde-format
msgid "Name:"
msgstr "Nom :"

#: qml/FeedGroupDialog.qml:50
#, kde-format
msgid "Description:"
msgstr "Description :"

#: qml/FeedListDelegate.qml:29
#, kde-format
msgid "%1 unread entry"
msgid_plural "%1 unread entries"
msgstr[0] "Entrée %1 non lue"
msgstr[1] "Entrées %1 non lues"

#: qml/FeedListDelegate.qml:35
#, kde-format
msgctxt "'Feed' is an rss feed"
msgid "Delete this Feed"
msgstr "Supprimer ce fil d'actualités"

#: qml/FeedListDelegate.qml:50
#, kde-format
msgctxt "'Feed' is an rss feed"
msgid "Edit this Feed"
msgstr "Modifier ce fil d'actualité"

#: qml/FeedListPage.qml:18
#, kde-format
msgctxt "'Feeds' as in 'RSS Feeds'"
msgid "Manage Feeds"
msgstr "Gérer les fils d'actualité"

#: qml/FeedListPage.qml:30
#, kde-format
msgid "Refresh All Feeds"
msgstr "Actualiser tous les fils d'actualités"

#: qml/FeedListPage.qml:36
#, kde-format
msgid "Add Feed…"
msgstr "Ajouter un fil d'actualités..."

#: qml/FeedListPage.qml:40
#, kde-format
msgctxt "@title"
msgid "Add Feed"
msgstr "Ajouter un flux"

#: qml/FeedListPage.qml:46
#, kde-format
msgid "Manage Feed Groups"
msgstr "Gérer les groupes de fils d'actualité"

#: qml/FeedListPage.qml:51
#, kde-format
msgid "Import Feeds…"
msgstr "Gérer des fils d'actualité"

#: qml/FeedListPage.qml:56
#, kde-format
msgid "Export Feeds…"
msgstr "Exporter des fils d'actualité"

#: qml/FeedListPage.qml:86
#, kde-format
msgid "No feeds added yet"
msgstr "Aucun fil d'actualité ajouté pour l'instant."

#: qml/FeedListPage.qml:124
#, kde-format
msgid "Import Feeds"
msgstr "Importer des flux"

#: qml/FeedListPage.qml:126
#, kde-format
msgid "All Files (*)"
msgstr "Tous les fichiers (*)"

#: qml/FeedListPage.qml:126
#, kde-format
msgid "XML Files (*.xml)"
msgstr "Fichiers « XML » (*.xml)"

#: qml/FeedListPage.qml:126
#, kde-format
msgid "OPML Files (*.opml)"
msgstr "Fichiers « OPML » (*.opml)"

#: qml/FeedListPage.qml:132
#, kde-format
msgid "Export Feeds"
msgstr "Exporter des flux"

#: qml/FeedListPage.qml:134
#, kde-format
msgid "All Files"
msgstr "Tous les fichiers"

#: qml/GroupsListPage.qml:18
#, kde-format
msgid "Groups"
msgstr "Groupes"

#: qml/GroupsListPage.qml:24
#, kde-format
msgid "Add Group…"
msgstr "Ajouter un groupe..."

#: qml/GroupsListPage.qml:55
#, kde-format
msgid "Remove"
msgstr "Supprimer"

#: qml/GroupsListPage.qml:63
#, kde-format
msgid "Set as Default"
msgstr "Définir par défaut"

#: qml/GroupsListPage.qml:72
#, kde-format
msgid "No groups created yet"
msgstr "Aucun fil d'actualité créé pour l'instant."

#: qml/GroupsListPage.qml:77
#, kde-format
msgid "Add Group"
msgstr "Ajouter un groupe"

#: qml/SettingsPage.qml:24
#, kde-format
msgctxt "@title"
msgid "Article List"
msgstr "Liste des articles"

#: qml/SettingsPage.qml:33
#, kde-format
msgid "Delete after:"
msgstr "Supprimer après :"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Never"
msgstr "Jamais"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Articles"
msgstr "Articles"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Days"
msgstr "Jours"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Weeks"
msgstr "Semaines"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Months"
msgstr "Mois"

#: qml/SettingsPage.qml:55
#, kde-format
msgid "Article"
msgstr "Article"

#: qml/SettingsPage.qml:62
#, kde-format
msgid "Font size:"
msgstr "Taille de la police :"

#: qml/SettingsPage.qml:71
#, kde-format
msgid "Use system default"
msgstr "Utiliser la valeur par défaut du système"

#~ msgid "Import Feeds..."
#~ msgstr "Importer des flux..."

#~ msgid "Export Feeds..."
#~ msgstr "Exporter des flux..."

#~ msgid "OK"
#~ msgstr "Ok"

#~ msgid "Configure Groups"
#~ msgstr "Configurer les groupes"

#~ msgid "Mark as unread"
#~ msgstr "Marquer comme non lu"

#~ msgid "Mark as read"
#~ msgstr "Marquer comme lu"

#~ msgid "Show all entries"
#~ msgstr "Afficher toutes les entrées"

#~ msgid "Show only unread entries"
#~ msgstr "N'afficher que les entrées non lues."

#~ msgid "Edit"
#~ msgstr "Modifier"

#~ msgid "Feed group"
#~ msgstr "Groupe de fils d'actualité"

#~ msgid "Add feed"
#~ msgstr "Ajouter un fil d'actualité"

#~ msgid "Add new Feed"
#~ msgstr "Ajouter un fil d'actualité"

#~ msgid "Posts"
#~ msgstr "Messages"

#~ msgid "Save"
#~ msgstr "Enregistrer"

#~ msgid "Url to add to the subscriptions"
#~ msgstr "URL à ajouter aux abonnements"

#~ msgid "Add"
#~ msgstr "Ajouter"
